import React from "react";
import emailjs from "emailjs-com";
import "./contact.css";

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      subject: "",
      phone: "",
      email: "",
      message: "",
      error_message: "",
    };
  }

  clear_state = () => {
    this.setState({
      name: "",
      subject: "",
      phone: "",
      email: "",
      message: "",
      error_message: "",
    });
  };

  handleSubmit = (event) => {
    const templateParams = {
      to_name: "Restaurant",
      from_name: this.state.name,
      subject: this.state.subject,
      from_phone: this.state.phone,
      from_email: this.state.reply_to,
      message: this.state.message,
    };

    const SERVICE_ID = "service_bjo6w46";
    const TEMPLATE_ID = "template_26eqjqk";
    const USER_ID = "user_JNxnXgveY7iG3ZHCqS1Rc";

    emailjs
      .send(SERVICE_ID, TEMPLATE_ID, templateParams, USER_ID)
      .then((response) => {
        console.log("SUCCESS", response.status, response.text);
      })
      .catch((error) => {
        console("FAILED", error);
      });

    this.clear_state();
    event.preventDefault();
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <div>
        <div>
          <span className="send-email-title">Have a question ? </span>
          <span className="send-email-subtitle"> Send us a message </span>
        </div>

        <div className="send-email-form col-md-6 offset-md-3">
          <form onSubmit={this.handleSubmit}>
            <label className="label-rest">
              Name:
              <input
                className="input-rest"
                type="text"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
                required
              />
            </label>
            <label className="label-rest">
              Subject:
              <input
                className="input-rest"
                type="text"
                name="subject"
                value={this.state.subject}
                onChange={this.handleChange}
              />
            </label>
            <label className="label-rest">
              Phone:
              <input
                className="input-rest"
                type="text"
                name="phone"
                value={this.state.phone}
                onChange={this.handleChange}
                required
              />
            </label>
            <label className="label-rest">
              email:
              <input
                className="input-rest"
                type="text"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
                required
              />
            </label>
            <label className="label-message">
              message:
              <textarea
                className="input-message"
                type="text"
                name="message"
                value={this.state.message}
                onChange={this.handleChange}
              />
            </label>
            <input className="input-submit" type="submit" value="Send" />
          </form>
          <span style={{ color: "red" }}>{this.state.error_message}</span>
        </div>
      </div>
    );
  }
}

export default Contact;
