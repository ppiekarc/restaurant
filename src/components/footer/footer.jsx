import React from "react";
import "./footer.css";

const contact_data = require("./contact_data.json");
const social_media_links = require("./social_media.json");

const facebook_icon =
  "https://uploads.sitepoint.com/wp-content/uploads/2018/11/1594256432facebook.jpg";
const instagram_icon =
  "https://uploads.sitepoint.com/wp-content/uploads/2018/11/1594256351instagram-by-facebook.jpg";
const twiter_icon =
  "https://uploads.sitepoint.com/wp-content/uploads/2018/11/1594256512twitter.jpg";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <footer>
        <div className="contact">
          <span className="footer-category-text"> CONTACT </span>
          <span className="footer-category-text-2ndlevel">
            {contact_data.adress.name}{" "}
          </span>
          <span className="footer-element">
            {contact_data.adress.street} {contact_data.adress.number},{" "}
            {contact_data.adress.post} {contact_data.adress.city}{" "}
          </span>
          <span className="footer-element">tel: {contact_data.phone} </span>
          <span className="footer-element">email: {contact_data.email} </span>
        </div>

        <div className="social-media">
          <a href={social_media_links.facebook}>
            <img className="social-img" src={facebook_icon} alt="" />
          </a>
          <a href={social_media_links.instagram}>
            <img className="social-img" src={instagram_icon} alt="" />
          </a>
          <a href={social_media_links.twiter}>
            <img className="social-img" src={twiter_icon} alt="" />
          </a>
        </div>
      </footer>
    );
  }
}

export default Footer;
