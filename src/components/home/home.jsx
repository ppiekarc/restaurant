import React from "react";
import "./home.css";

const home_content = require("./home_data.json");

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const first_image = home_content.carousel_images[0];
    const rest_images = home_content.carousel_images.slice(1);
    return (
      <div>
        <h3> {home_content.description_title} </h3>
        <br></br>
        <p className="description"> {home_content.description}</p>
        <br></br>
        <h3> {home_content.carousel_title}</h3>

        {/* @TODO Mayby move this to another class / function */}
        <div id="demo" className="carousel slide" data-ride="carousel">
          {/* Indicators */}
          <ul className="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" className="active"></li>
            {rest_images.map((image, idx) => (
              <li data-target="#demo" data-slide-to={idx + 1}></li>
            ))}
          </ul>

          {/* The slideshow  */}
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                className="carousel-image img-fluid"
                src={first_image}
                alt=""
              />
            </div>
            {rest_images.map((image) => (
              <div className="carousel-item">
                <img className="carousel-image img-fluid" src={image} alt="" />
              </div>
            ))}
          </div>

          {/* Left and right controls */}
          <a className="carousel-control-prev" href="#demo" data-slide="prev">
            <span className="carousel-control-prev-icon"></span>
          </a>
          <a className="carousel-control-next" href="#demo" data-slide="next">
            <span className="carousel-control-next-icon"></span>
          </a>
        </div>
      </div>
    );
  }
}

export default Home;
