import React from "react";
import "./menu.css";

const menu_dishes = require("./menu_data.json");

class MenuElement extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const imgAlt = "Missing image of dish";
    return (
      <div className="menuElement">
        <h2>{this.props.name}</h2>
        <p className="description">{this.props.description}</p>
        <img className="dish_image" src={this.props.img} alt={imgAlt} />
        <p className="price">{this.props.price}</p>
      </div>
    );
  }
}

class Menu extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        {menu_dishes.map((dish) => (
          <MenuElement
            name={dish.name}
            description={dish.description}
            img={dish.img}
            price={dish.price}
          />
        ))}
      </div>
    );
  }
}

export default Menu;
