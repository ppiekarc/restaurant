import React from "react";
import { Link } from "react-router-dom";
import "../index.css";
import { LINKS } from "../index";

class Nav extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <nav className="navbar bg-dark navbar-dark navbar-expand-sm">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#mainmenu"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="mainmenu">
            <ul className="navbar-nav">
              {Object.keys(LINKS).map((key, idx) => (
                <Link to={LINKS[key]}>
                  <li key={idx} className="nav-link">
                    {key}
                  </li>
                </Link>
              ))}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Nav;
