import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./index.css";
import Nav from "./components/nav";
import Home from "./components/home/home";
import Menu from "./components/menu/menu";
import About from "./components/about";
import Contact from "./components/contact/contact";
import Footer from "./components/footer/footer";

export const LINKS = {
  Home: "/",
  About: "/about",
  Menu: "/menu",
  Contact: "/contact",
};

class Index extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Router>
          <header>
            <Nav />
          </header>
          <main>
            <Switch>
              <Route path={LINKS["About"]} component={About} />
              <Route path={LINKS["Menu"]} component={Menu} />
              <Route path={LINKS["Contact"]} component={Contact} />
              <Route path={LINKS["Home"]} component={Home} />
            </Switch>
          </main>
        </Router>
        <Footer />
      </div>
    );
  }
}

ReactDOM.render(<Index />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
